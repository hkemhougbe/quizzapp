import 'dart:io';

import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:quizzapp/core/error/failure.dart';
import 'package:quizzapp/data/models/requests/question_request.dart';
import 'package:quizzapp/data/models/responses/question_response.dart';

class RemoteApi {
  static const String url = 'https://opentdb.com/api.php';

  Future<List<QuestionResponse>> getQuestions(QuestionRequest resquest) async {
    try {
      final response = await Dio().get(url, queryParameters: resquest.toJson());
      if (response.statusCode == 200) {
        final data = Map<String, dynamic>.from(response.data);
        final results = List<Map<String, dynamic>>.from(data["results"]);
        if (results.isNotEmpty) {
          return results.map((e) => QuestionResponse.fromJson(e)).toList();
        }
      }
      return [];
    } on DioError catch (e) {
      print(e);
      throw Failure(message: e.message);
    } on SocketException catch (e) {
      print(e);
      throw const Failure(message: "please check your connection");
    }
  }
}
